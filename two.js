/* Author Ani Hovhannisyan */

var net = require('net');

var client = new net.Socket();
var crud = {
      'c': 'CREATE'
    , 'r': 'READ'
    , 'u': 'UPDATE'
    , 'd': 'DELETE'
}

function sendCreate () {
    var r = {
          'cmd': crud.c
        , 'singer' : 'Parov Stelar'
    };
    client.write(JSON.stringify(r));
}

function sendRead () {
    var r = {
          'cmd': crud.r
        , 'singer': 'Parov Stelar'
    };
    client.write(JSON.stringify(r));
}

function sendUpdate () {
    var r = {
          'cmd': crud.u
        , 'singer': 'Parov Stelar'
        , 'last_album': 'The Burning Spider'
        , 'web_site': 'http://www.parovstelar.com/#contact'
        , 'last_video': 'https://www.youtube.com/watch?v=TQVTQdyTod4'
    };
    client.write(JSON.stringify(r));
}

function sendDelete () {
    var r = {
          'cmd': crud.d
        , 'singer' : 'Parov Stelar'
    };
    client.write(JSON.stringify(r));
}

function handleReceivedData (data) {
    switch (data.cmd) {
        case crud.c: {
            console.log('Sending: READ');
            sendUpdate();
            break;
        } case crud.u: {
            console.log('Sending: UPDATE');
            sendRead();
            break;
        } case crud.r: {
            console.log('Sending: Delete');
            sendDelete();
            break;
        } case crud.d: {
            console.log('Sending: Disconnect');
            client.destroy();
            break;
        } default : {
            console.log('Nothing to do yet');
            break;
        }
    }
}

client.connect(1111, '127.0.0.1', function() {
    console.log('Connected');
    sendCreate();
});

client.on('data', function(data) {
    console.log('Received: ' + data);
    //Some testing flow to Read, Update, Delete and disconnect
    data = JSON.parse(data);
    handleReceivedData(data);
});

client.on('close', function() {
    console.log('Connection closed');
});
