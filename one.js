/* Author Ani Hovhannisyan */

var fs = require('fs');
var net = require('net');

var singersFile = 'singers.json';
var crud = {
      'c': 'CREATE'
    , 'r': 'READ'
    , 'u': 'UPDATE'
    , 'd': 'DELETE'
}

function getSingerTemplate () {
    var w = {
          'name': ''
        , 'last_album': ''
        , 'web_site': ''
        , 'last_video': ''
    };
    return w;
}

function getResponseTemplate (d) {
    var res = {
         'cmd': d.cmd
       , 'msg': 'No message'
    };
    return res;
}

function handleCreate (res, singers, data) {
    var w = getSingerTemplate();
    w['name'] = data.singer;
    singers[w.name] = w;
    res.msg = 'Created new singer by name ' + w.name;
}

function handleUpdate (res, singers, data) {
    singers[data.singer]['last_album'] = data.last_album;
    singers[data.singer]['web_site'] = data.web_site;
    singers[data.singer]['last_video'] = data.last_video;
    res.msg = 'Updated singer by name ' + data.singer;
}

function handleDelete (res, singers, data) {
    delete singers[data.singer];
    res.msg = 'Deleted singer by name ' + data.singer;
}

function handleCommands (s, d) {
    var res = getResponseTemplate(d);
    switch (d.cmd) {
        case crud.c: {
            handleCreate(res, s, d);
            break;
        } case crud.r: {
            res.msg = s[d.singer];
            break;
        } case crud.u: {
            handleUpdate(res, s, d);
            break;
        } case crud.d: {
            handleDelete(res, s, d);
            break;
        } default : {
            break;
        }
    }
    return res;
}

function handleReceivedData (socket, data) {
    data = JSON.parse(data);
    console.log('\nReceived command ' + data.cmd);
    //TODO: Handle safely singer's infos updates
    var singers = JSON.parse(fs.readFileSync(singersFile, 'utf8'));
    var res = handleCommands(singers, data);
    console.log('---------------------------------------------------');
    console.log('Singers are \n\t' + JSON.stringify(singers));
    console.log('---------------------------------------------------');
    fs.writeFileSync(singersFile, JSON.stringify(singers), 'utf8');
    socket.write(JSON.stringify(res));
}

var server = net.createServer(function(socket) {
    socket.name = socket.remoteAddress + ':' + socket.remotePort
        fs.writeFileSync(singersFile, '{}');
    console.log('\nConnected new server ' + socket.name + '\n');

    socket.on('data', function (data) {
        handleReceivedData(socket, data);
    });

    socket.on('end', function () {
        console.log('\nEnded socket connection ' + socket.name + '\n');
    });
});

server.listen(1111, 'localhost');
